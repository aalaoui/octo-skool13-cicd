import { describe, it } from 'mocha'
import { expect } from 'chai'
import { sayHello } from './say-hello'

describe('sayHello()', () => {
  it('says hello', () => {
    expect(sayHello('foo')).to.equal('Hello foo!')
  })
})
